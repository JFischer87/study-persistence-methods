# Estudos de Persistência de Dados
Este projeto tem como objetivo principal entender de forma prática conceitos
relacionado a persistência de dados em um banco de dados.

Iremos fazer uma abordagem comparativa entre 3 implementações para entendermos
como funcionam, os pontos positivos e negativos de cada implementação, bem como
cuidados que devemos tomar em cada uma delas.

Abaixo temos as três implementações que ieremos realizar:

1. Java puro usando JDBC para persistir em bando de dados.
2. Java com um ORM (Object Relation Mapper) + JPA.
3. Java com Spring Data.

Iremos utilizar o Spring Boot para facilitar a criação camada de aplicação para
realizarmos as requisições HTPP, assim como adotaremos uma arquitetura hexagonal
simplemente como objeto de estudo.

## Páginas 
1. [JDBC](./jdbc/README.md)
2. [Hibernate + JPA](./hibernate-jpa/README.md)
3. [Spring Data JPA](./jdbc/README.md)


## Pré-requisitos
- Java 21
- Docker

## Arquitetura Hexagonal
O conceito de Arquitetura Hexagonal foi proposto por Alistair Cockburn, em meados 
dos anos 90, onfr a ideia é construir sistemas que favorecem reusabilidade de código, 
alta coesão, baixo acoplamento, independência de tecnologia e que são mais fáceis 
de serem testados.

Uma Arquitetura Hexagonal divide as classes de um sistema em dois grupos principais:
- Classes de domínio, isto é, diretamente relacionadas com o negócio do sistema.
- Classes relacionadas com infraestrutura, tecnologias e responsáveis pela integração 
com sistemas externos (tais como bancos de dados).

Representação de uma arquitetura hexagonal:
![hexagonal-architecture.png](assets%2Fhexagonal-architecture.png)

As camadas são isoladas através de interfaces ou portas, que garantem o desacoplamento.

O lado esquerdo do hexagono é ator principal, o lado do usuário, que conduz uma ação, 
pois este é o lado do usuário que realiza alguma tarefa.

O centro do hexagono é onde ficam localizados os modelos, domínios e regras de negócios de seu software.
É um ambiente que deve ser totalmente isolado em termos de não ser afetado por ocorrências 
externas, por exemplo, o banco de dados que será utilizado, framework frontend.

O lado direito do hexagono é o ator secundário, lado dos dados, que é conduzido, seja para 
escrever dados, ler dados, modificar dados, e apagar dados.