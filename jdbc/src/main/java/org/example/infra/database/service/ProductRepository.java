package org.example.infra.database.service;

import lombok.extern.slf4j.Slf4j;
import org.example.infra.api.v1.dto.PaginationDto;
import org.example.domain.model.Product;
import org.example.domain.port.ProductRepositoryPort;
import org.example.infra.api.v1.dto.ProductDto;
import org.example.infra.database.config.TransactionManager;
import org.springframework.stereotype.Repository;

import java.nio.ByteBuffer;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Slf4j
@Repository
public class ProductRepository implements ProductRepositoryPort {

  private final TransactionManager databaseConnection;

  public ProductRepository(TransactionManager databaseConnection) {
    this.databaseConnection = databaseConnection;
  }
  @Override
  public Product add(ProductDto productDto){
    String sql = "INSERT INTO product (id, description, price) VALUES (?, ?, ?)";

    Product product = null;

    try {
      Connection connection = databaseConnection.getConnection();
      PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

      UUID uuid = UUID.randomUUID();
      byte[] id = uuidToByteArray(uuid);

      preparedStatement.setBytes(1, id);
      preparedStatement.setString(2, productDto.getDescription());
      preparedStatement.setDouble(3, productDto.getPrice().doubleValue());

      int rowsAffected = preparedStatement.executeUpdate();
      if (rowsAffected > 0) {
        product = Product.builder()
            .id(uuid)
            .description(productDto.getDescription())
            .price(productDto.getPrice())
            .build();
        log.debug("Generated: " + product.toString());
      }
    }catch (SQLException e){
      log.error(e.toString());
    }
    return product;
  }

  @Override
  public List<Product> list(PaginationDto paginationDto) {
    String sql = "SELECT * FROM mysql_db.product LIMIT ? OFFSET ?";
    List<Product> products = new ArrayList<>();

    try {
      Connection connection = databaseConnection.getConnection();
      PreparedStatement preparedStatement = connection.prepareStatement(sql);
      preparedStatement.setInt(1, paginationDto.getSize());
      preparedStatement.setInt(2, paginationDto.getOffset());

      try(ResultSet resultSet = preparedStatement.executeQuery()) {
        while (resultSet.next()){
          byte[] uuidBytes = resultSet.getBytes("id");
          UUID id = byteArrayToUUID(uuidBytes);
          products.add(Product.builder()
                  .id(id)
                  .description(resultSet.getString("description"))
                  .price(resultSet.getDouble("price"))
              .build());
        }
      }
    }catch (SQLException e) {
      log.error(e.toString());
    }

    return products;
  }

  private UUID byteArrayToUUID(byte[] bytes) {
    ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
    long high = byteBuffer.getLong();
    long low = byteBuffer.getLong();
    return new UUID(high, low);
  }

  private byte[] uuidToByteArray(UUID uuid) {
    ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[16]);
    byteBuffer.putLong(uuid.getMostSignificantBits());
    byteBuffer.putLong(uuid.getLeastSignificantBits());
    return byteBuffer.array();
  }
}
