package org.example.infra.database.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
@Slf4j
@Configuration
public class TransactionManager {
  @Value("${jdbc.url}")
  private String url;
  @Value("${jdbc.uername}")
  private String userName;
  @Value("${jdbc.password}")
  private String password;

  private Connection connection;
  public Connection getConnection() {
    try {
      if(connection == null){
        connection = DriverManager.getConnection(
            url, userName, password
        );
      }
    }catch (SQLException e){
      log.error(e.toString());
    }
    return connection;
  }
}
