package org.example.domain.usecase;

import org.example.exception.FieldValidationException;
import org.example.infra.api.v1.dto.PaginationDto;
import org.example.domain.model.Product;
import org.example.domain.port.ProductPort;
import org.example.domain.port.ProductRepositoryPort;
import org.example.infra.api.v1.dto.ProductDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductUseCase implements ProductPort {
  private final ProductRepositoryPort productRepositoryPort;
  public ProductUseCase(ProductRepositoryPort productRepositoryPort) {
    this.productRepositoryPort = productRepositoryPort;
  }

  @Override
  public ProductDto add(ProductDto productDto){

    if(null == productDto.getPrice()){
      throw new FieldValidationException("Price must be not null");
    }

    if(productDto.getDescription().isBlank()){
      throw new FieldValidationException("Price must be not null or empty");
    }

    Product product = productRepositoryPort.add(productDto);


    return ProductDto.builder()
        .description(product.getDescription())
        .price(product.getPrice())
        .build();
  }

  @Override
  public List<ProductDto> list(PaginationDto paginationDto) {

    List<Product> products = productRepositoryPort.list(paginationDto);

    return products.stream()
        .map(p -> ProductDto.builder()
            .description(p.getDescription())
            .price(p.getPrice())
            .build())
        .toList();
  }
}
