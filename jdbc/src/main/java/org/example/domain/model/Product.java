package org.example.domain.model;

import lombok.*;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Product {
  private UUID id;
  private String description;
  private Double price;

  @Override
  public String toString() {
    return "Product{" +
        "id:" + id +
        ", description:'" + description + '\'' +
        ", price:" + price +
        '}';
  }
}
