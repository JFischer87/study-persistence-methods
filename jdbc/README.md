# Persistência com JDBC
O objetivo deste projeto é demonstrar de maneira rapida e simples a implementação de uma
aplicação que irá realizar a persistência em um banco de dados, neste exemplo MySql, 
utilizando apenas o JDBC para isto.

## Dependências
Neste projeto iremos utilizar as seguintes dependências:
- Spring Boot Start Web - Para criarmos uma rota HTTP para realizarmos a solicitação de
ou busca dos produtos na base.
- MySql Connector J - É o driver JDBC que permite que aplicativos JAVA se conectem ao banco
de dados MySql, executem consultas SQL e manipulem dados armazenados no banco.
- Lombok - Fornece annotation úteis que nos permitem acelerar o desenvolvimento da aplicação.

## Desenvolvimento
O projeto foi construindo utilizando uma arquitetura hexagonal onde temos um pacote para separar
as clases que são pertinentes ao dominio da aplicação e regras de negócio e uma outra camada de
infra que irá conter as classes de acesso e persistência da aplicação.

A seguir iremos detalhar um pouco mais cada um dos pacotes.

### Config
Temos duas classes para levantarmos os tempos de cada requisição e realizarmos os logs,
através das classes `RequestHandlerInterceptor` e `CustomInterceptor`.

A classe `CustomInterceptor` implementa a classe `WebMvcConfigurer` que é uma interface do
Spring MVC usada para configurar o comportamento do controlador da web. Isso indica que
esta classe é responsável por configurar interceptadores para requisições web.

O método `addInterceptors` é o método que estamos substituindo da interface WebMvcConfigurer.
Ele é chamado pelo Spring durante a inicialização da aplicação para adicionar interceptadores.

A classe `RequestHandlerInterceptor` implementa a interface `HandlerInterceptor`, o que
significa que ela pode ser usada como um interceptor para pré e pós-processar requisições HTTP.

### Domain
Dentro da camada de dominio temos os pacotes `model`, `port` e `usecase`, onde:
`model` - temos a representação do nosso produto
`port` - temos as interfaces que são responsáveis por desacoplar a camada de domain da camada 
de infra.
`usecase` - é onde temos as regras de negócio da nossa aplicação.

### Infra
Dentro do pacote de infra temos dois pacotes `api` e `database`, onde:
- `api` - contem a classe `ProductController` que expoem ao usuário os endpoinds de para listar
e adicionar produtos novos. Veja que nossa camada de api está completamente isolada da camada de
domínio através dos ports.
- `database` - de forma análoga a camada de api temos o isolamento da camada de domain através do
port.

Como o foco deste estudo é entender mais como funciona a persitência utilizando apenas o JDBC, 
iremos detalhar um pouco mais a classe `ProductRepository`.

#### ProductRepository
Dentro da classe `ProductRepositoru` temos dois métodos que serão responsáveis por salvar um novo
produto e listar os produtos da basse, são eles `add` e `list` respectivamente. Ambos tem básicamente
a mesma implementação, mudando um pouco apenas a query que iremos executar no banco.

Através da classe `Connection` do proprio JAVA e utilizando a classe `DatabaseConnection` que criamos
estabelecemos a conexão com o banco de dados.

Utilizando a classe `PreparedStatement` passamos o SQL e os parâmetros, para posteriormente executarmos
os comandos `executeUpdate()` e `executeQuery()`, onde o primeiro é responsável por realizar ações de
modificações no banco como UPDATE, INSERT ou DELETE e o segundo é usado para buscar informações do banco
através do INSERT.

#### DatabaseConnection

Conforme mencionado anteriormente a classe `DatabaseConnection` é usada para criar a conexão com o
banco de dados.

## Pontos Positivos
- Compatibilidade: é suportado por uma ampla gama de bancos de dados relacionais.
- Desempenho: altamente eficiente e oferece um bom desempenho para operações de banco de dados, 
especialmente quando usado corretamente e com cuidado para evitar gargalos de desempenho.
- Controle total: oferece aos desenvolvedores um alto nível de controle sobre as consultas SQL 
e as transações do banco de dados. Isso permite otimizar consultas e transações para atender às 
necessidades específicas da aplicação.
- Segurança: oferece recursos para ajudar a proteger contra ataques de injeção SQL e outras 
vulnerabilidades de segurança comuns, quando usado corretamente, como o uso de consultas parametrizadas.

## Pontos Negativos
- Complexidade: pode ser mais complexo do que algumas outras abstrações de acesso a banco de dados 
disponíveis em frameworks de alto nível. Requer mais código para realizar operações básicas, o que 
pode aumentar a complexidade e o tempo de desenvolvimento.
- Manutenção de código: exige que os desenvolvedores escrevam e mantenham manualmente o código SQL, 
o que pode ser propenso a erros e exigir mais esforço de desenvolvimento e teste para garantir a 
correção e a eficiência das consultas.
- Gerenciamento de Conexão: gerenciamento manual de conexões pode ser propenso a erros e exigir 
cuidados extras para garantir que as conexões sejam adequadamente abertas e fechadas para evitar 
vazamentos de recursos ou problemas de desempenho.
- Curva de aprendizado: a curva de aprendizado pode ser íngreme, especialmente para aqueles que 
não têm experiência prévia com SQL ou acesso a banco de dados em Java.