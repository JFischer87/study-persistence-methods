package org.example.infra.api.v1.dto;

import lombok.extern.slf4j.Slf4j;
import org.example.exception.FieldValidationException;

@Slf4j
public class PaginationDto {
  private static final int DEFAULT_PAGE = 1;
  private static final int DEFAULT_SIZE = 10;
  private Integer page;
  private Integer size;
  public PaginationDto(Integer page, Integer size) {
    if(page < 0){
      String message = "Page number must be greater than or equal to 0";
      log.error(message);
      throw new FieldValidationException(message);
    }

    if(size < 0){
      String message = "Size number must be greater than or equal to 0";
      log.error(message);
      throw new FieldValidationException(message);
    }

    this.page = (page != null) ? page : DEFAULT_PAGE;
    this.size = (size != null) ? size : DEFAULT_SIZE;
  }
  public Integer getPage() {
    return page;
  }
  public Integer getSize() {
    return size;
  }
  public Integer getOffset() {
    return page * size;
  }
}
