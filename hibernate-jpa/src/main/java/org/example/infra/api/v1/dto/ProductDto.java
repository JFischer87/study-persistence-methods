package org.example.infra.api.v1.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
  private String description;
  private Double price;
}
