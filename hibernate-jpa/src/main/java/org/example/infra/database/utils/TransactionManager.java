package org.example.infra.database.utils;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import org.example.infra.api.v1.dto.PaginationDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class TransactionManager {


  private final EntityManagerFactory entityManagerFactory;

  public TransactionManager() {
    this.entityManagerFactory = Persistence
        .createEntityManagerFactory("jpa-hibernate");
  }
  public <T> void save(T entity){
    EntityManager entityManager = this.entityManagerFactory.createEntityManager();
    try {
      entityManager.getTransaction().begin();
      entityManager.persist(entity);
      entityManager.getTransaction().commit();
    }catch (Exception e){
      log.error(e.toString());
      entityManager.getTransaction().rollback();
    }finally {
      entityManager.close();
    }
  }

  public <T> List<T> list(PaginationDto paginationDto, Class<T> entityClass){
    List<T> list = new ArrayList<>();
    EntityManager entityManager = this.entityManagerFactory.createEntityManager();
    String jpql = "SELECT e FROM "+ entityClass.getSimpleName() + " e";
    try {
      entityManager.getTransaction().begin();

      TypedQuery<T> query = entityManager.createQuery(jpql, entityClass)
          .setFirstResult(paginationDto.getOffset())
          .setMaxResults(paginationDto.getSize());
      list = query.getResultList();

      entityManager.getTransaction().commit();
    }catch (Exception e){
      log.error(e.toString());
      entityManager.getTransaction().rollback();
    }finally {
      entityManager.close();
    }
    return list;
  }

}
