package org.example.infra.database.service;

import lombok.extern.slf4j.Slf4j;
import org.example.infra.api.v1.dto.PaginationDto;
import org.example.domain.model.Product;
import org.example.domain.port.ProductRepositoryPort;
import org.example.infra.api.v1.dto.ProductDto;
import org.example.infra.database.entity.ProductEntity;
import org.example.infra.database.utils.TransactionManager;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Repository
public class ProductRepository implements ProductRepositoryPort {
  @Override
  public Product add(ProductDto productDto){

    ProductEntity productEntity = ProductEntity.builder()
        .description(productDto.getDescription())
        .price(productDto.getPrice())
        .build();

    try {
      TransactionManager transactionManager = new TransactionManager();
      transactionManager.save(productEntity);
    }catch (Exception e) {
      log.error(e.toString());
    }

    return Product.builder()
      .id(productEntity.getId())
      .description(productEntity.getDescription())
      .price(productEntity.getPrice())
      .build();
  }

  @Override
  public List<Product> list(PaginationDto paginationDto) {
    List<Product> products = new ArrayList<>();
    try {
      TransactionManager transactionManager = new TransactionManager();
      var list = transactionManager.list(paginationDto, ProductEntity.class);
      products.addAll(list.stream().map(p -> Product.builder()
              .id(p.getId())
              .description(p.getDescription())
              .price(p.getPrice())
              .build())
          .toList());
    }catch (Exception e) {
      log.error(e.toString());
    }
    return products;
  }
}
