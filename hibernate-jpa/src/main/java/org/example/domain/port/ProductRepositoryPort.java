package org.example.domain.port;

import org.example.infra.api.v1.dto.PaginationDto;
import org.example.domain.model.Product;
import org.example.infra.api.v1.dto.ProductDto;

import java.util.List;

public interface ProductRepositoryPort {
  Product add(ProductDto product);
  List<Product> list(PaginationDto paginationDto);
}
