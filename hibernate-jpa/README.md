# Persistência com Hibernate e JPA
O objetivo deste projeto é demonstrar de maneira rápida e simples a implementação de uma
aplicação que irá realizar a persistência em um banco de dados, neste exemplo MySql, 
utilizando apenas o Hibernate e JPA para isto.

## Dependências
Neste projeto iremos utilizar as seguintes dependências:
- Spring Boot Start Web - Para criarmos uma rota HTTP para realizarmos a solicitação de
- Lombok - Fornece annotation úteis que nos permitem acelerar o desenvolvimento da aplicação.
- hibernate-core - É o núcleo do framework Hibernate, que é uma implementação da especificação 
JPA (Java Persistence API).
- hibernate-c3p0 - É um pacote que fornece integração entre o Hibernate e o c3p0, que é uma
biblioteca de pooling de conexão para Java
- mysql-connector-java - É um driver JDBC para o MySQL, ele implementa a interface JDBC padrão
do JAVA.

## Desenvolvimento
O projeto foi construindo utilizando uma arquitetura hexagonal onde temos um pacote para separar
as clases que são pertinentes ao dominio da aplicação e regras de negócio e uma outra camada de
infra que irá conter as classes de acesso e persistência da aplicação.

A seguir iremos detalhar um pouco mais cada um dos pacotes.

### Config
Temos duas classes para levantarmos os tempos de cada requisição e realizarmos os logs,
através das classes `RequestHandlerInterceptor` e `CustomInterceptor`.

A classe `CustomInterceptor` implementa a classe `WebMvcConfigurer` que é uma interface do
Spring MVC usada para configurar o comportamento do controlador da web. Isso indica que
esta classe é responsável por configurar interceptadores para requisições web.

O método `addInterceptors` é o método que estamos substituindo da interface WebMvcConfigurer.
Ele é chamado pelo Spring durante a inicialização da aplicação para adicionar interceptadores.

A classe `RequestHandlerInterceptor` implementa a interface `HandlerInterceptor`, o que
significa que ela pode ser usada como um interceptor para pré e pós-processar requisições HTTP.

As configurações da conexão com o banco de dados estão presentes no arquivo `persistence.xml`.

### Domain
Dentro da camada de dominio temos os pacotes `model`, `port` e `usecase`, onde:
`model` - temos a representação do nosso produto
`port` - temos as interfaces que são responsáveis por desacoplar a camada de domain da camada 
de infra.
`usecase` - é onde temos as regras de negócio da nossa aplicação.

### Infra
Dentro do pacote de infra temos dois pacotes `api` e `database`, onde:
- `api` - contem a classe `ProductController` que expoem ao usuário os endpoinds de para listar
e adicionar produtos novos. Veja que nossa camada de api está completamente isolada da camada de
domínio através dos ports.
- `database` - de forma análoga a camada de api temos o isolamento da camada de domain através do
port.

Como o foco deste estudo é entender mais como funciona a persitência utilizando apenas o JDBC, 
iremos detalhar um pouco mais a classe `ProductRepository`.

#### ProductRepository
Dentro da classe `ProductRepositoru` temos dois métodos que serão responsáveis por salvar um novo
produto e listar os produtos da basse, são eles `add` e `list` respectivamente. Ambos tem básicamente
a mesma implementação, mudando um pouco apenas a query que iremos executar no banco.

Através da classe `TransactionManager` estabelecemos a conexão com o banco de dados e através,
dos métodos `save` e `list` realizamos as transações de adição de um produto e listagem dos produtos. 


Utilizando a classe `PreparedStatement` passamos o SQL e os parâmetros, para posteriormente executarmos
os comandos `executeUpdate()` e `executeQuery()`, onde o primeiro é responsável por realizar ações de
modificações no banco como UPDATE, INSERT ou DELETE e o segundo é usado para buscar informações do banco
através do INSERT.

#### TransactionManager
A clase foi criada de forma genérica podendo ser reaproveitada por qualquer outra entidade. Nela temoos
a criação de um `EntityManagerFactory` que geralmente é criado uma única vez durante o ciclo de vida da
aplicação e é utilizada para criar multiplas instâncias de `EntityManager`, é pesada e custo para criar
então é uma boa prática criarmos apenas uma vez e reutilizarmos em toda a aplicação.

Já a `EntityManager` criamos para cada operação e encerramos após utilizarmos. 

## Pontos Positivos
- Produtividade: Hibernate e JPA fornecem uma abstração sobre o banco de dados, o que pode aumentar a 
produtividade do desenvolvedor, permitindo que eles se concentrem mais na lógica de negócios em vez de 
lidar diretamente com SQL.
- Portabilidade: Como Hibernate e JPA são especificações padrão da Java EE, as aplicações desenvolvidas 
com essas tecnologias são mais portáteis entre diferentes fornecedores de banco de dados.
- Gerenciamento de Transações: Hibernate e JPA oferecem suporte para gerenciamento de transações, 
garantindo que as operações no banco de dados sejam atômicas e consistentes.
- Cache de Segundo Nível: Hibernate fornece um cache de segundo nível, que pode melhorar 
significativamente o desempenho da aplicação ao reduzir a necessidade de acessar o banco de dados para 
recuperar dados frequentemente acessados.

## Pontos Negativos
- Curva de Aprendizado: Hibernate e JPA têm uma curva de aprendizado íngreme, especialmente para 
desenvolvedores inexperientes.
- Desempenho: Embora Hibernate e JPA possam simplificar o desenvolvimento, eles também podem introduzir 
sobrecarga de desempenho, especialmente se não forem usados corretamente.
- Abstração Excessiva: A abstração fornecida pelo Hibernate e JPA pode ocultar detalhes importantes do 
banco de dados. Isso pode ser problemático em casos onde é necessário um controle fino sobre as 
consultas SQL executadas pelo sistema.
- Bloqueio de Versão: Dependendo da versão do Hibernate e JPA utilizada, pode haver problemas de 
compatibilidade com versões mais recentes do Java ou do banco de dados.