package org.example.infra.database.service;

import lombok.extern.slf4j.Slf4j;
import org.example.infra.api.v1.dto.PaginationDto;
import org.example.domain.model.Product;
import org.example.domain.port.ProductRepositoryPort;
import org.example.infra.api.v1.dto.ProductDto;
import org.example.infra.database.entity.ProductEntity;
import org.example.infra.database.repository.ProductRepository;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Primary
@Component("productRepoImpl")
public class ProductRepositoryImpl implements ProductRepositoryPort {
  private final ProductRepository productRepository;
  public ProductRepositoryImpl(ProductRepository productRepository) {
    this.productRepository = productRepository;
  }

  @Override
  public Product add(ProductDto productDto){

    ProductEntity productEntity = ProductEntity.builder()
        .description(productDto.getDescription())
        .price(productDto.getPrice())
        .build();

    ProductEntity product = productRepository.save(productEntity);

    return Product.builder()
      .id(product.getId())
      .description(product.getDescription())
      .price(product.getPrice())
      .build();
  }
  @Override
  public List<Product> list(PaginationDto paginationDto) {

    Pageable pageable = PageRequest.of(paginationDto.getPage(), paginationDto.getSize());

    Page<ProductEntity> productsEntity = productRepository.findAll(pageable);

    return new ArrayList<>(productsEntity.stream()
        .map(p -> Product.builder()
            .id(p.getId())
            .description(p.getDescription())
            .price(p.getPrice())
            .build())
        .toList());
  }
}
