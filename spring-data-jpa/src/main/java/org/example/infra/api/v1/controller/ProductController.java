package org.example.infra.api.v1.controller;

import org.example.infra.api.v1.dto.PaginationDto;
import org.example.domain.port.ProductPort;
import org.example.infra.api.v1.dto.ProductDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/products")
public class ProductController {

  final ProductPort productPort;

  public ProductController(final ProductPort productPort) {
    this.productPort = productPort;
  }
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public ProductDto add(@RequestBody final ProductDto product) {
    return productPort.add(product);
  }

  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  public List<ProductDto> list(@RequestParam(required = false) final Integer page,
                               @RequestParam(required = false) final Integer size) {
    return productPort.list(new PaginationDto(page, size));
  }
}
