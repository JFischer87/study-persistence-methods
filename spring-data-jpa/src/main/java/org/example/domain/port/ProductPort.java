package org.example.domain.port;

import org.example.infra.api.v1.dto.PaginationDto;
import org.example.infra.api.v1.dto.ProductDto;

import java.util.List;

public interface ProductPort {

  ProductDto add(ProductDto product);
  List<ProductDto> list(PaginationDto paginationDto);
}
